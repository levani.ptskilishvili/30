s = "titanic"

function task1(){
    document.write("სიტყვის სიგრძეა - "+s.length)
}

task1()

function task2(){
    let countA = s.split("a")
    document.write("'a' ს რაოდენობა სიტყვაში არის - "+(countA.length-1))
}
document.write("<br><br>")
task2()

function task3(){
    let countA = s.split("an")
    document.write("number of 'an' in text is "+(countA.length-1))
}
document.write("<br><br> task3 <br>")
task3()

function task4(){
    for (i=0; i<s.length; i++) {
        document.write(s.charAt(i)+'-'+s.charCodeAt(i)+"&emsp;")
    }
}
document.write("<br><br> task4 <br>")
task4()

function task5(){
    for (i=1; i<=40; i++) {
        document.write(String.fromCharCode(""+(Math.floor(Math.random()*(123-97))+97)+""))
    }
}
document.write("<br><br> task5 <br>")
task5()

function task6(n){
    for (i=1; i<=n; i++) {
        document.write(String.fromCharCode(""+(Math.floor(Math.random()*(123-97))+97)+""))
    }
}
document.write("<br><br> task6 <br>")
task6(10)

function task7(n){
    for(q=1;q<=20;q++){
        for (i=1; i<=n; i++) {
            document.write(String.fromCharCode(""+(Math.floor(Math.random()*(123-97))+97)+""))
        }        
        document.write("&emsp;")
    }
}
document.write("<br><br> task7 <br>")
task7(10)