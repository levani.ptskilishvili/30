function createGreenSquare() {
    const square = document.createElement('div');
    square.className = 'green-square';

    const maxX = 480; // (500 - 20) to ensure the square stays within the container
    const maxY = 480; // (500 - 20)

    const randomX = Math.floor(Math.random() * maxX);
    const randomY = Math.floor(Math.random() * maxY);

    square.style.left = `${randomX}px`;
    square.style.top = `${randomY}px`;

    document.getElementById('container').appendChild(square);
}

setInterval(createGreenSquare, 200);