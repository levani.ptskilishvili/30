function getRandomColor() {
    const colors = ['yellow', 'green', 'blue', 'red'];
    const randomIndex = Math.floor(Math.random() * colors.length);
    return colors[randomIndex];
  }

  function createSquare() {
    const square = document.createElement('div');
    square.className = 'square ' + getRandomColor();
    return square;
  }

  function populateField() {
    const field = document.getElementById('field');
    const numSquares = (500 / 50) * (600 / 50); // Calculate the number of squares that can fit
    for (let i = 0; i < numSquares; i++) {
      const square = createSquare();
      square.style.left = (i % (500 / 50)) * 50 + 'px';
      square.style.top = Math.floor(i / (500 / 50)) * 50 + 'px';
      field.appendChild(square);
    }
  }

  populateField();