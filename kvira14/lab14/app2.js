const canvas = document.getElementById('sadCanvas');
const ctx = canvas.getContext('2d');

// Draw the face
ctx.beginPath();
ctx.arc(100, 100, 80, 0, 2 * Math.PI);
ctx.strokeStyle = '#888'; // Grey stroke
ctx.lineWidth = 5;
ctx.stroke();
ctx.closePath();

// Draw the left eye
ctx.beginPath();
ctx.arc(70, 80, 10, 0, 2 * Math.PI);
ctx.strokeStyle = '#888'; // Grey stroke
ctx.stroke();
ctx.closePath();

// Draw the right eye
ctx.beginPath();
ctx.arc(130, 80, 10, 0, 2 * Math.PI);
ctx.strokeStyle = '#888'; // Grey stroke
ctx.stroke();
ctx.closePath();


// Draw the mouth (upward curve, lower position)
ctx.beginPath();
ctx.arc(100, 140, 40, 1.2 * Math.PI, 1.8 * Math.PI);
ctx.strokeStyle = '#888'; // Grey stroke
ctx.lineWidth = 5;
ctx.stroke();
ctx.closePath();
