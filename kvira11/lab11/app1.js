function generateRandomNumbers() {
    var n = parseInt(document.getElementById('n').value);
    var k = parseInt(document.getElementById('k').value);
    var p = parseInt(document.getElementById('p').value);
    var outputDiv = document.getElementById('output');
    outputDiv.innerHTML = '';

    for (var i = 0; i < n; i++) {
      var randomNumber = Math.floor(Math.random() * (p - k + 1)) + k;
      outputDiv.innerHTML += 'Random Number ' + (i + 1) + ': ' + randomNumber + '<br>';
    }
  }