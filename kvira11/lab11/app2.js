function move(direction) {
    const circle = document.getElementById('circle');
    
    let currentMarginTop = parseInt(circle.style.marginTop, 10) || 0;
    let currentMarginLeft = parseInt(circle.style.marginLeft, 10) || 0;

    switch(direction) {
      case 'up':
        currentMarginTop -= 10;
        break;
      case 'down':
        currentMarginTop += 10;
        break;
      case 'left':
        currentMarginLeft -= 10;
        break;
      case 'right':
        currentMarginLeft += 10;
        break;
    }

    // Ensure the circle stays within the container
    currentMarginTop = Math.max(0, currentMarginTop);
    currentMarginLeft = Math.max(0, currentMarginLeft);

    circle.style.marginTop = `${currentMarginTop}px`;
    circle.style.marginLeft = `${currentMarginLeft}px`;
  }